import React, { Component } from "react";
import "./home.css";
import List from "./list";
import Details from "./details";
import model from "../assets/img/model.jpg";

class Home extends Component {
  products = [
    {
      id: 1,
      price: 30,
      name: "GUCCI G8850U",
      url: "./img/v1.png",
      desc: "My mentors are kool",
    },
    {
      id: 2,
      price: 50,
      name: "GUCCI G8759H",
      url: "./img/v2.png",
      desc: "My mentors are insane",
    },
    {
      id: 3,
      price: 30,
      name: "DIOR D6700HQ",
      url: "./img/v3.png",
      desc: "My mentors are lovable",
    },
    {
      id: 4,
      price: 30,
      name: "DIOR D6005U",
      url: "./img/v4.png",
      desc: "My mentors are helpful",
    },
    {
      id: 5,
      price: 30,
      name: "PRADA P8750",
      url: "./img/v5.png",
      desc: "My mentors are kind",
    },
    {
      id: 6,
      price: 30,
      name: "PRADA P9700",
      url: "./img/v6.png",
      desc: "My mentors are supportive",
    },
    {
      id: 7,
      price: 30,
      name: "FENDI F8750",
      url: "./img/v7.png",
      desc: "My mentors are always here 4 me",
    },
    {
      id: 8,
      price: 30,
      name: "FENDI F8500",
      url: "./img/v8.png",
      desc: "Thank you my dear friends",
    },
    
  ];

  state = {
    selectedProduct: null,
  };

  setSelectedProduct = (clickedProduct) => {
    this.setState({
      selectedProduct: clickedProduct,
    });
  };
  //-----------------//-----------------
  render() {
    return (
      <div className="background">
        <h1 className="py-3 text-white">TRY GLASSES APP ONLINE</h1>
        {this.state.selectedProduct ? (
          <Details selectedProduct={this.state.selectedProduct} />
        ) : (<div className="m-auto p-3 imaged ">
          <img className="innerpic" style={{width: "37%"}} src={model} alt="" />
        </div>)}
        
        <List
          products={this.products}
          setSelectedProduct={this.setSelectedProduct}
        />
      </div>
    );
  }
}

export default Home;
