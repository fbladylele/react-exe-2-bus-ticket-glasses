import React, { Component } from "react";

class Item extends Component {
  //-----------------//-----------------
  render() {
    const { url } = this.props.prod;
    return (
      <div>
        <a
          onClick={() => this.props.setSelectedProduct(this.props.prod)}
          className="text-dark"
          href="#"
        >
          <img style={{width: "120%"}} src={url} alt="img" />
        </a>
      </div>
    );
  }
}

export default Item;
