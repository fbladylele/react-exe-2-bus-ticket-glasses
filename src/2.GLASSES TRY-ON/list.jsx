import React, { Component } from "react";
import Item from "./item";
import "./list.css";

class List extends Component {
  renderProduct = () => {
    return this.props.products.map((item) => {
      return (
        <div key={item.id} className="col-1 m-auto">
          <Item
            prod={item}
            setSelectedProduct={this.props.setSelectedProduct}
          />
        </div>
      );
    });
  };
  //-----------------//-----------------
  render() {
    return (
      <div className="container list">
        <div className="row">
          {this.renderProduct()}
        </div>
      </div>
    );
  }
}

export default List;
