import React, { Component } from "react";
import "./details.css";
import model from "../assets/img/model.jpg";

class Details extends Component {
  //-----------------//-----------------
  render() {
    const {desc, url} = this.props.selectedProduct;
    return (
      <div className="wear">
        <div className="m-auto p-3 image">
          <img className="bg-img"  src={model} alt="" />
          <img className="glasses" src={url} alt="glasses" />
          <div className="desc">  {desc} </div>
        
        </div>
      </div>
    );
  }
}

export default Details;
