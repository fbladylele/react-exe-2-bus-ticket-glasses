import React, { Component } from "react";
import SeatItem from "./seatItem";

class SeatList extends Component {
  renderSeats = () => {
    return this.props.seats.map((item) => {
      const isBooking =
        this.props.cart.findIndex((i) => i.SoGhe === item.SoGhe) !== -1;
      return (
        <SeatItem
          key={item.TenGhe}
          singleSeat={item}
          addToCart2={this.props.addToCart}
          isBooking={isBooking}
        />
      );
    });
  };

  render() {
    return (
      <div className="container">
        <div className="row mx-0">{this.renderSeats()}</div>
      </div>
    );
  }
}

export default SeatList;