import React, { Component } from "react";

class Cart extends Component {
  renderCart = () => {
    return this.props.cart.map((item) => {
      const { SoGhe, Gia } = item;
      return (
        <p>
          Ghế số: {SoGhe} <span> - Giá: ${Gia}</span>{" "}
          <a
            onClick={() => this.props.delete(SoGhe)}
            className="text-danger px-4"
            href="#"
          >
            [Hủy]
          </a>
        </p>
      );
    });
  };
  render() {
    return <div>{this.renderCart()}</div>;
  }
}

export default Cart;


