import React, { Component } from "react";

class SeatItem extends Component {
  render() {
    const { TrangThai, SoGhe } = this.props.singleSeat;
    if (TrangThai) {
      return (
        <>
          <div className="col-3 my-1 px-0">
            <button
              className="border-0 rounded bg-danger text-light"
              disabled
              style={{ width: 50, height: 50 }}
            >
              {SoGhe}
            </button>
          </div>
        </>
      );
    } else {
      return (
        <div className="col-3 my-1 px-0">
          <button
            onClick={() => this.props.addToCart2(this.props.singleSeat)}
            className={`border-0 rounded ${
              this.props.isBooking ? "bg-success" : "bg-dark"
            } text-light`}
            style={{ width: 50, height: 50 }}
          >
            {SoGhe}
          </button>
        </div>
      );
    }
  }
}

export default SeatItem;
